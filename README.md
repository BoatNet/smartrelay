## SmartRelay
The SmartRelay is designed as a 'remote' switching device aboard. The board contains PowerFETs for switching DC loads (so not reallya Relay baord). When a device connetced to the SmartRelay stops consuming power, then it will send a notification to the network (assuming someting is wrong). The Relay should also be capable of measuring power used by the device.

## Description
This board is develop as a HAT for the [Hatlabs Sailor HAT with ESP32](https://hatlabs.github.io/sh-esp32/). The main features of the board are:
- 4 fused outputs (5A max)
- ATO style fuses
- manual override by moving ATO fuse
- INA219 to measure power
- Supports NMEA and SignalK

Rendering of a rev 0.0.1 board:

Front side.
![Rev 0.0.1 board](SH-ESP32-SmartRelay.png)

3D render of the board
![3D](SH-ESP32-SmartRelay-3D.png)


## Authors and acknowledgment
This design is inspired by the [SH-ESP32-PowerFET](https://github.com/markfarnan/yacht_hardware/tree/main/SH-ESP32-PowerFET) board from Mark Farnan


## Project status
This project is not finished yet. First batch of boards is being manufactered.

## Todo
- Add blown fuse detection
- Add AL5809 LED drivers 
- Add 24V capability


