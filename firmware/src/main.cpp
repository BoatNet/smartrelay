#include <Arduino.h>

#include "sensesp_app.h"
#include "sensesp_app_builder.h"
#include "sensors/digital_output.h"
#include "signalk/signalk_listener.h"
#include "signalk/signalk_output.h"
#include "signalk/signalk_put_request_listener.h"
#include "transforms/repeat_report.h"
#include "sensors/ina219.h"

// This example implements a smart light switch which can control a load either via
// a manual button press, or via Signal K PUT requests.

// Control devices are wired to the following digital pins
#ifdef ESP32
#define PIN_RELAY1 12 
#define PIN_RELAY2 13
#define PIN_RELAY3 14
#define PIN_RELAY4 15
#else
#define PIN_RELAY1 D5
#define PIN_RELAY2 D6
#define PIN_RELAY3 D7
#define PIN_RELAY4 D8
#endif

void setupSmartSwitch(const int outputPin, const String sk_path, const int ina, const String config_path)
{
  auto* load_switch = new DigitalOutput(outputPin);

  // Here, we set up a SignalK PUT request listener to handle
  // requests made to the Signal K server to set the switch state.
  // This allows any device on the SignalK network that can make
  // such a request to also control the state of our switch.

  auto* sk_listener = new SKBoolPutRequestListener(sk_path + ".state");
  sk_listener->connect_to(load_switch);


  // Finally, connect the load switch to an SKOutput so it reports its state 
  // to the Signal K server.  Since the load switch only reports its state 
  // whenever it changes (and switches like light switches change infrequently), 
  // send it through a `RepeatReport` transform, which will cause the state 
  // to be reported to the server every 10 seconds, regardless of whether 
  // or not it has changed.  That keeps the value on the server fresh and 
  // lets the server know the switch is still alive.


  load_switch->connect_to(new RepeatReport<bool>(10000, config_path+"/repeat"))
             ->connect_to(new SKOutputBool(sk_path + ".state", config_path+"/path"));

  // Create an INA219, which represents the physical sensor.
  // 0x40 is the default address. Chips can be modified to use 0x41 (shown
  // here), 0x44, or 0x45. The default volt and amp ranges are 32V and 2A
  // (cal32_2). Here, 32v and 1A is specified with cal32_1.
  auto* ina219 = new INA219(ina, cal32_1);


  // report current (amperage).
  auto* ina219_current = new INA219Value(ina219, INA219Value::current, 5000);
  ina219_current->connect_to(new SKOutputNumber(sk_path + ".current", config_path+"/current"));
}


// SensESP builds upon the ReactESP framework. Every ReactESP application
// defines an "app" object (vs. defining a "main()" method).
ReactESP app([]() {

// Some initialization boilerplate when in debug mode...
#ifndef SERIAL_DEBUG_DISABLED
  SetupSerialDebug(115200);
#endif

  // Create a builder object
  SensESPAppBuilder builder;

  // Create the global SensESPApp() object.
  sensesp_app = builder.set_hostname("sk-smart-relay")
                    ->set_sk_server("10.10.1.102", 3000)
                    ->set_wifi("0xDEADBEEF", "XXXX")
                    ->set_standard_sensors(StandardSensors::NONE)
                    ->get_app();


  // Define the SK Path that represents the load this device controls.
  // This device will report its status on this path, as well as
  // respond to PUT requests to change its status.
  // To find valid Signal K Paths that fits your need you look at this link:
  // https://signalk.org/specification/1.4.0/doc/vesselsBranch.html  
  const char* sk_path[] = {"electrical.switches.anchorLight"
                         , "electrical.switches.runningLight"
                         , "electrical.switches.mastHeadLight"
                         , "electrical.switches.deckLight"};


  // "Configuration paths" are combined with "/config" to formulate a URL
  // used by the RESTful API for retrieving or setting configuration data.
  // It is ALSO used to specify a path to the SPIFFS file system
  // where configuration data is saved on the MCU board.  It should
  // ALWAYS start with a forward slash if specified.  If left blank,
  // that indicates a sensor or transform does not have any
  // configuration to save.
  //const char* config_path_sk_output = "/signalk/path";
  //const char* config_path_repeat = "/signalk/repeat";

  setupSmartSwitch(PIN_RELAY1, String(sk_path[0]), 0x40, String("/signalk/Relay_1"));
  setupSmartSwitch(PIN_RELAY2, String(sk_path[1]), 0x41, String("/signalk/Relay_2"));
  setupSmartSwitch(PIN_RELAY3, String(sk_path[2]), 0x42, String("/signalk/Relay_3"));
  setupSmartSwitch(PIN_RELAY4, String(sk_path[3]), 0x43, String("/signalk/Relay_4"));

  // Start the SensESP application running
  sensesp_app->enable();

});
